const del = require('del');
const { series, src, dest, watch } = require('gulp');
const path = require('path');
const fs = require('fs');
const sass = require('gulp-sass');

function clean(cb) {
  del(['./public/stylesheets/*', './public/javascripts/*']);
  cb();
}

function css(cb) {
  src('./src/stylesheets/*.scss')
    .pipe(sass({}).on('error', sass.logError))
    .pipe(dest('./public/stylesheets/'));
  cb();
}


function js(cb) {
  src('src/scripts/*.js')
    .pipe(dest('./public/scripts'));
  cb();
}

let updateEverything = series(clean, css, js);

let watcher = watch('./src/**/*.*');

watcher.on('all', function(event, path, stats) {
  console.log('File ' + path + ' was ' + event + ', running tasks...');
  updateEverything();
});


function defaultTask(cb) {
  updateEverything();
  cb();
}

exports.default = defaultTask;